module.exports = (req, res, next) => {
  if (req.session && req.session.user) {
    // sharing globaly between all pages with nunjucks
    res.locals.user = req.session.user
    return next()
  }

  return res.redirect('/')
}
